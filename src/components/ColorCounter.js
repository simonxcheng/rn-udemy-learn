import React, { useState } from 'react';
import {View, Text, Button, Image, StyleSheet} from 'react-native';

function adjustColorValue(colorValue) {
    return Math.min(255, Math.max(0, colorValue));
}

const ColorCounter = (props) => {
    console.log("rendering "+ props.colorName);
    return (
        <View>
            <Text style={styles.text} > 
                Adjust {props.colorName} {props.colorValue}
            </Text>
            <Button
                style={styles.button}
                title= {'More ' + props.colorName}
                onPress={() => {
                    console.log("More clicked")
                    props.onColorChanged(10)
                    }} > 
            </Button>

            <Button
                style={styles.button}
                title= {'Less ' + props.colorName}
                onPress={() => props.onColorChanged(-10)}> 
            </Button>
        </View>
    );
};

const styles = StyleSheet.create({
    text: {fontSize: 30},
    button: {marginBottom: 10}
});

// const comparisonFn = function(prevProps, nextProps) {

//     var result = prevProps.colorName === nextProps.colorName &&
//     prevProps.colorValue === prevProps.colorValue;

//     console.log("comparisonFn " + prevProps + " " + nextProps)
//     return result;
//   };

export default React.memo(ColorCounter);