import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

const ImageDetail = (props) => {
    console.log(props);
    return (
        <View>
            <Image source={props.imageSource}></Image>
            <Text style={styles.text}>{props.title}</Text>
            <Text style={styles.text}>Image score - {props.imageScore}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    text: {
      fontSize: 24
    }
  });

export default ImageDetail;