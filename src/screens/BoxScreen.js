import React, { useState } from 'react';
import {View, Text, StyleSheet, TextInput, Button, FlatList} from 'react-native';


const BoxScreen = () => {
    return (<View style={styles.viewStyle}>
        {/* <Text style={styles.text1Style}>Child #1</Text>
        <Text style={styles.text2Style}>Child #2</Text>
        <Text style={styles.text3Style}>Child #3</Text> */}
        <View style={styles.view1Style}></View>
        <View style={styles.view2Style}></View>
        <View style={styles.view3Style}></View>
    </View>);
}

const styles = StyleSheet.create({
    parentStyle:{
        paddingTop: 20
    },
    viewStyle: {
        paddingTop: 20,
        flexDirection: 'column',
        borderWidth: 1,
        borderColor: 'black',
        margin:10,
        marginVertical: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 100
    },
    view1Style: {
        height: 50,
        width: 50,
        backgroundColor: 'red'
    },
    view2Style: {
        height: 50,
        width: 50,
        backgroundColor: 'green',
        alignSelf: 'flex-end'
    },
    view3Style: {
        height: 50,
        width: 50,
        backgroundColor: 'purple'
    },
    text1Style: {
        borderWidth: 1,
        borderColor: 'red',
        alignSelf: 'center',
        flex: 3
    },
    text2Style: {
        borderWidth: 1,
        borderColor: 'red',
        alignSelf: 'flex-start',
        flex: 3
    },
    text3Style: {
        borderWidth: 1,
        borderColor: 'red',
        flex: 1
    }
});

export default BoxScreen;