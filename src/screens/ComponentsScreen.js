import React from 'react';
import {Text, StyleSheet, View} from 'react-native';


const ComponentsScreen = () => {
    const name = "Simon";
    return (<View>
            <Text style={styles.textStyleTitle}>Getting started with React Native</Text>
            <Text style={styles.textStyle}>My name is {name}</Text>
        </View>)
};

const styles = StyleSheet.create( {
    textStyleTitle: {
        fontSize: 45
    },
    textStyle: {
        fontSize: 20
    },
    
});

export default ComponentsScreen;