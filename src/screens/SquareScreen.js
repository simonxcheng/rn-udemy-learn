import React, { useReducer } from 'react';
import {View, Text, StyleSheet, Button, FlatList} from 'react-native';
import ColorCounter from '../components/ColorCounter'

function adjustColorValue(colorValue) {
    return Math.min(255, Math.max(0, colorValue));
}

const SquareScreen = (props) => {
    const reducer = (state, action) => {
        //console.log("Reducer is called " + action.type)
        // state === {r, g, b}
        // action === { type: 'change_red', 'change_green', 'change_blue', payload:10}
        switch(action.type) {
            case 'change_red':
                // never change state value directly
                return {...state, red: adjustColorValue(state.red + action.payload)}
            case 'change_green':
                return {...state, green: adjustColorValue(state.green + action.payload)}
            case 'change_blue':
                return {...state, blue: adjustColorValue(state.blue + action.payload)}
            default:
                return state
        }
    };

    const [state, dispatch] = useReducer(reducer, {red: 0, green: 0, blue: 0})
    console.log(state)

    var result = (
        <View>
            <ColorCounter colorName='Red' colorValue={state.red} key={'Red'+ state.red}
                onColorChanged={
                    (newColor) => {
                        console.log("Red onColorChanged is called " + newColor)
                        dispatch({type:'change_red', payload:newColor})
                    }
                }
            > 
            </ColorCounter>

            <ColorCounter colorName='Green' colorValue={state.green} key={'Green'+ state.green}
                onColorChanged={(newColor) => dispatch({type:'change_green', payload:newColor})}
            > 
            </ColorCounter>

            <ColorCounter colorName='Blue' colorValue={state.blue} key={'Blue' + state.blue}
                onColorChanged={(newColor) => dispatch({type:'change_blue', payload:newColor})}
            > 
            </ColorCounter>

            <View style={
                {height: 100, 
                 width: 100, 
                 backgroundColor: `rgb(${state.red}, ${state.green}, ${state.blue})`}
            } />
        </View>
    );

    console.log("============ end of render =============");
    return result;
}

const styles = StyleSheet.create({
    text: {
      fontSize: 40
    }
  });

export default SquareScreen;