import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import ImageDetail from '../components/ImageDetail';

const ImageScreen = () => {
    return (
        <View>
            <ImageDetail title='Forest' 
                imageSource={require("../../assets/forest.jpg")}
                imageScore='9' />
            <ImageDetail title='Beach'
                imageScore='8'
                imageSource = {require('../../assets/beach.jpg')}/>
            <ImageDetail title='Mountain'
                imageScore='7'
                imageSource = {require('../../assets/mountain.jpg')}/>
        </View>
    );
}

const styles = StyleSheet.create({
    text: {
      fontSize: 40
    }
  });

export default ImageScreen;