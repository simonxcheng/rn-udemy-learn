import React, { useReducer } from 'react'

import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";

const CounterScreen = () => {
    const reducer = (state, action) => {
        switch(action.type) {
            case 'up':
                return {value: state.value + action.payload};
            case 'down':
                return {value: state.value - action.payload};
        }
    }
    const [state, dispatch] = useReducer(reducer, {value: 0})
    
    return (
    <View>
        <Button title="Increase" onPress={()=>{
            dispatch({type:'up', payload:1})
            }}/>
        <Button title="Decrease" onPress={()=>{
            dispatch({type:'down', payload:1})
            }}/>
        <Text>Current Count: {state.value}</Text>
    </View>
    );
}

const styles = StyleSheet.create({
    text: {
      fontSize: 40
    }
  });

export default CounterScreen;

